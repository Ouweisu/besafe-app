install:
	docker compose -f ./docker/docker-compose.yml up -d --build
	docker exec -it besafe-app-container npm install
.PHONY: install

start:
	docker exec -it besafe-app-container npx expo start
.PHONY: start

exec:
	docker exec -it besafe-app-container bash
.PHONY: exec
